package com.wms.common.core.constant;

/**
 * 服务名称
 * 
 * @author wms
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "wms-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "wms-system";


    /**
     * 进销存模块的serviceid
     */
    public static final String OMS_SERVICE = "wms-oms";

    /**
     * 商城模块的serviceid
     */
    public static final String SHOP_SERVICE = "wms-shop";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "wms-file";
}
