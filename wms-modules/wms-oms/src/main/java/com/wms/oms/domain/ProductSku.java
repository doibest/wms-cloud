package com.wms.oms.domain;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.wms.oms.handlers.SpecAttrListHandler;
import lombok.Data;
import com.wms.common.core.web.domain.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 商品sku信息对象 wms_product_sku
 *
 * @author zzm
 * @date 2021-05-15
 */
@Data
@TableName("wms_product_sku")
public class ProductSku extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 商品编号 */
    private String sn;

    /** 商品条码 */
    private String barCode;

    /** 商品sku名称 */
    private String skuName;

    /** 规格 */
    private String specifications;


    /** 商品id */
    private Long productId;

    /** 零售价 */
    @NotNull(message = "零售价不能为空")
    @Digits(integer = 20,fraction = 4,message = "请输入有效数字")
    @DecimalMin(value = "0",message = "零售价必须是数字且必须不小于0")
    private BigDecimal salePrice;

    /** 批发价 */
    @NotNull(message = "批发价不能为空")
    @Digits(integer = 20,fraction = 4,message = "请输入有效数字")
    @DecimalMin(value = "0",message = "批发价必须是数字且必须不小于0")
    private BigDecimal tradePrice;

    /** 市场价 */
    @Digits(integer = 20,fraction = 4,message = "请输入有效数字")
    @DecimalMin(value = "0",message = "市场价必须是数字且必须不小于0")
    private BigDecimal marketPrice;

    /** 折扣一 */
    private BigDecimal discount1;

    /** 折扣二 */
    private BigDecimal discount2;

    /** 预计采购价 */
    @Digits(integer = 20,fraction = 4,message = "请输入有效数字")
    @DecimalMin(value = "0",message = "预计采购价必须是数字且必须不小于0")
    private BigDecimal purchasePrice;

    /** 图片 */
    private String images;


    /** 删除标志（0代表存在 1代表删除） */
    private Boolean delFlag;

    /** 租户ID */
    private Long tenantId;


}