package com.wms.oms.controller.weixin;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.wms.common.core.domain.R;
import com.wms.common.core.utils.bean.BeanUtils;
import com.wms.common.core.web.controller.BaseController;
import com.wms.common.core.web.domain.AjaxResult;
import com.wms.oms.domain.Product;
import com.wms.oms.domain.ProductSku;
import com.wms.oms.domain.dto.SpecAttrDto;
import com.wms.oms.domain.dto.SpecDto;
import com.wms.oms.domain.dto.WxProductDto;
import com.wms.oms.service.IProductService;
import com.wms.oms.service.IProductSkuService;
import com.wms.shop.api.RemoteCartService;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

/**
 * 微信小程序调用商品接口
 */

@RestController
@RequestMapping("/wx/product")
public class ProductWxController extends BaseController {


    @Autowired
    private IProductService productService;

    @Autowired
    private IProductSkuService productSkuService;

    @Autowired
    private RemoteCartService remoteCartService;

    /**
     * 查询商品DTO信息列表
     */
    @GetMapping("/list")
    public AjaxResult list(Product product){
        List<Product> list = productService.selectProductList(product);
        return AjaxResult.success(list);
    }


    /**
     * 查询商品详情
     * @param id
     * @return
     */
    @GetMapping(value = "/getProduct")
    public AjaxResult getProduct(Long id)
    {
        Product product = productService.selectWxProductById(id);
        R<JSONObject> remoteRes =  remoteCartService.selectCartByProductId(id);
        Assert.notNull(product, "商品不能为空!");
        WxProductDto wxProductDto = new WxProductDto();
        BeanUtils.copyProperties(product,wxProductDto);
        JSONArray jsonArray = (JSONArray) JSONArray.parse(JSON.toJSONString(product.getSpecList()));
        List<SpecDto> specDtoList = Lists.newArrayList();
        for (Object obj : jsonArray) {
            SpecDto specDto = new SpecDto();
            JSONObject jsonObject = (JSONObject) JSONObject.parse(obj.toString());
            specDto.setName(jsonObject.get("specName").toString());
            String specAttrStr = jsonObject.get("specAttr").toString();
            List<String> specAttrList = JSONArray.parseArray(specAttrStr, String.class);
            List<SpecAttrDto> specAttrDtoList = Lists.newArrayList();
            for (String attr : specAttrList) {
                SpecAttrDto specAttrDto = new SpecAttrDto();
                specAttrDto.setAttrName(attr);
                BigDecimal qty = BigDecimal.ONE;
                if (remoteRes.getCode() == R.SUCCESS && remoteRes.getData() != null && StringUtils.isNotEmpty(remoteRes.getData().getString("spec"))) {
                    if (attr.equals(remoteRes.getData().getString("spec"))) {
                        specAttrDto.setChecked(Boolean.TRUE);
                        qty = new BigDecimal(remoteRes.getData().getString("quantity"));
                    }
                }
                specAttrDto.setQty(qty);
                specAttrDtoList.add(specAttrDto);
            }
            specDto.setSpecAttrDtoList(specAttrDtoList);
            specDtoList.add(specDto);
        }
        wxProductDto.setSpecDtoList(specDtoList);
        return AjaxResult.success(wxProductDto);
    }


    /**
     * 查询商品详情
     * @param id
     * @return
     */
    @GetMapping(value = "/getProductSku")
    public AjaxResult getProductSku(Long id)
    {
        ProductSku productSku = productSkuService.selectById(id);
        Product product = productService.selectWxProductById(productSku.getProductId());
        WxProductDto wxProductDto = new WxProductDto();
        BeanUtils.copyProperties(product,wxProductDto);
        JSONArray jsonArray = (JSONArray) JSONArray.parse(JSON.toJSONString(product.getSpecList()));
        List<SpecDto> specDtoList = Lists.newArrayList();
        for (Object obj : jsonArray) {
            SpecDto specDto = new SpecDto();
            JSONObject jsonObject = (JSONObject) JSONObject.parse(obj.toString());
            specDto.setName(jsonObject.get("specName").toString());
            String specAttrStr = jsonObject.get("specAttr").toString();
            List<String> specAttrList = JSONArray.parseArray(specAttrStr, String.class);
            List<SpecAttrDto> specAttrDtoList = Lists.newArrayList();
            for (String attr : specAttrList) {
                SpecAttrDto specAttrDto = new SpecAttrDto();
                specAttrDto.setAttrName(attr);
                if (attr.contains(productSku.getSpecifications())) {
                    specAttrDto.setChecked(Boolean.TRUE);
                }
                specAttrDtoList.add(specAttrDto);
            }
            specDto.setSpecAttrDtoList(specAttrDtoList);
            specDtoList.add(specDto);
        }
        wxProductDto.setSpecDtoList(specDtoList);
        return AjaxResult.success(wxProductDto);
    }
}
