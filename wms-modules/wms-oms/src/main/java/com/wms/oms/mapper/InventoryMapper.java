package com.wms.oms.mapper;

import com.wms.oms.domain.Inventory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.math.BigDecimal;

/**
 * 库存信息Mapper接口
 *
 * @author zzm
 * @date 2021-05-16
 */
public interface InventoryMapper extends BaseMapper<Inventory>
{

    /**
     * 根据商品id查询商品库存
     * @param productId
     * @return
     */
    BigDecimal getProductQty(Long productId);

}