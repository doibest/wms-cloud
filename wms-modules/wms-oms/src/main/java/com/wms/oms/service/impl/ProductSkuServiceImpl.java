package com.wms.oms.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.wms.oms.mapper.ProductSkuMapper;
import com.wms.oms.domain.ProductSku;
import com.wms.oms.service.IProductSkuService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

/**
 * 商品sku信息Service业务层处理
 *
 * @author zzm
 * @date 2021-05-15
 */
@Service
public class ProductSkuServiceImpl implements IProductSkuService
{
    @Autowired
    private ProductSkuMapper productSkuMapper;


    /**
     * 查询商品sku信息列表
     *
     * @param productSku 商品sku信息
     * @return 商品sku信息
     */
    @Override
    public List<ProductSku> selectProductSkuList(ProductSku productSku)
    {
        QueryWrapper queryWrapper = new QueryWrapper();
        return productSkuMapper.selectList(queryWrapper);
    }

    @Override
    public ProductSku selectById(Long id) {
        return productSkuMapper.selectById(id);
    }



}