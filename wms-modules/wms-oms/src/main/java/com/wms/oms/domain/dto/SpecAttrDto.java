package com.wms.oms.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 商品规格属性（微信小程序显示）
 */

@Data
public class SpecAttrDto implements Serializable {

    private String attrName;

    private Boolean checked;

    private BigDecimal qty;
}
