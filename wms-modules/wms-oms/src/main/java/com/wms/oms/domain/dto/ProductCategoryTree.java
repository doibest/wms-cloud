package com.wms.oms.domain.dto;

import com.wms.common.core.domain.TreeNode;
import lombok.Data;

import java.time.LocalDateTime;


@Data
public class ProductCategoryTree extends TreeNode {

    /**
     * （1：开启；0：关闭）
     */
    private String enable;
    /**
     * 父分类编号
     */
    private Long parentId;
    /**
     * 名称
     */
    private String categoryName;
    /**
     * 描述
     */
    private String description;
    /**
     * 图片
     */
    private String picUrl;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 最后更新时间
     */
    private LocalDateTime updateTime;
    /**
     * 逻辑删除标记（0：显示；1：隐藏）
     */
    private Boolean delFlag;

}
