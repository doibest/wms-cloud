package com.wms.oms.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 规格名称
 */
@Data
public class SpecDto implements Serializable {

    private String name;

    private List<SpecAttrDto> specAttrDtoList;
}
