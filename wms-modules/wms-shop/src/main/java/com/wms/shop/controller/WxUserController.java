package com.wms.shop.controller;


import com.wms.common.core.domain.R;
import com.wms.common.core.web.controller.BaseController;
import com.wms.shop.api.model.WxLoginDTO;
import com.wms.shop.api.domain.WxUser;
import com.wms.shop.api.model.WxLoginUser;
import com.wms.shop.service.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



/**
 * 商城用户Controller
 * 
 * @author zzm
 * @date 2021-08-18
 */
@RestController
@RequestMapping("/wx/user")
public class WxUserController extends BaseController
{
    @Autowired
    private IUserService userService;


    /**
     * 获取商城用户详细信息
     */
    @PostMapping(value = "/info")
    public R<WxLoginUser>  getInfo(@RequestBody WxLoginDTO wxLoginDTO)
    {
        if (StringUtils.isBlank(wxLoginDTO.getJsCode())) {
            return R.fail("empty code!");
        }
        WxUser wxUser = userService.wxLogin(wxLoginDTO.getAppId(), wxLoginDTO.getJsCode());
        wxUser.setTenantId(wxLoginDTO.getTenantId());
        WxLoginUser wxLoginUser = new WxLoginUser();
        wxLoginUser.setWxUser(wxUser);
        return R.ok(wxLoginUser);
    }


}
