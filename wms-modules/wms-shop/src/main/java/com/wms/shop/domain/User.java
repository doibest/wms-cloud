package com.wms.shop.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.wms.common.core.web.domain.BaseEntity;
import lombok.Data;

/**
 * 商城用户对象 shop_user
 * 
 * @author zzm
 * @date 2021-08-18
 */
@Data
@TableName("shop_user")
public class User extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 微信openid */
    private String openid;

    /** 用户名称 */
    private String userName;

    /** 用户手机号 */
    private String phone;

    /** 删除标志（0代表存在 1代表删除） */
    private Boolean delFlag;

}
