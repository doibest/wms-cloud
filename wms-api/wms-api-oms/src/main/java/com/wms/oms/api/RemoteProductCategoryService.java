package com.wms.oms.api;

import com.wms.common.core.constant.ServiceNameConstants;
import com.wms.common.core.domain.R;
import com.wms.oms.api.factory.RemoteProductCategoryFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(contextId = "remoteProductCategoryService", value = ServiceNameConstants.OMS_SERVICE, fallbackFactory = RemoteProductCategoryFallbackFactory.class)
public interface RemoteProductCategoryService {

    /**
     * 查询所有当前租户下的分类
     * @return
     */
    @GetMapping(value = "/wx/category/firstList")
    R<List> getProductCategoryList();
}
