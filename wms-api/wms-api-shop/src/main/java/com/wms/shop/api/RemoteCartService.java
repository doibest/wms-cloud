package com.wms.shop.api;

import com.alibaba.fastjson.JSONObject;
import com.wms.common.core.constant.ServiceNameConstants;
import com.wms.common.core.domain.R;
import com.wms.shop.api.factory.RemoteCartFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author zzm
 * @date 2022/11/9
 * @desc
 */
@FeignClient(contextId = "RemoteCartService", value = ServiceNameConstants.SHOP_SERVICE, fallbackFactory = RemoteCartFallbackFactory.class)
public interface RemoteCartService {


    /**
     * 根据商品id查询选中规格
     * @param productId
     * @return
     */
    @GetMapping(value = "/wx/cart/selectCartByProductId")
    R<JSONObject> selectCartByProductId(@RequestParam("productId") Long productId);
}
