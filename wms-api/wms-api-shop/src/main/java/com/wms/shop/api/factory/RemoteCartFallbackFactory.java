package com.wms.shop.api.factory;

import com.alibaba.fastjson.JSONObject;
import com.wms.common.core.domain.R;
import com.wms.shop.api.RemoteCartService;
import com.wms.shop.api.RemoteWxUserService;
import com.wms.shop.api.model.WxLoginDTO;
import com.wms.shop.api.model.WxLoginUser;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 查询购物车选中规格降级处理
 */
@Component
@Slf4j
public class RemoteCartFallbackFactory implements FallbackFactory<RemoteCartService> {

    @Override
    public RemoteCartService create(Throwable throwable) {
        log.error("查询购物车选中规格:{}", throwable.getMessage());
        return new RemoteCartService() {
            @Override
            public R<JSONObject> selectCartByProductId(Long productId) {
                return R.fail("查询购物车选中规格失败:" + throwable.getMessage());
            }
        };
    }
}
